# Create image from maven image (https://hub.docker.com/_/maven/)
FROM maven:3-jdk-8

# Get repository files into the container
ADD . /devops-pipeline-assignment-rborges

# Building the application..
WORKDIR /devops-pipeline-assignment-rborges
RUN mvn install
RUN mvn compile